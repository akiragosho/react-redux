import { createSlice } from '@reduxjs/toolkit';

const initialPhotos = [
  {
    id: 1,
    categoryId: 5,
    photo: 'https://picsum.photos/id/532/500/500',
    title: 'Enim laboris dolore consectetur et fugiat do amet eiusmod anim proident do culpa irure consectetur.'
  },
  {
    id: 2,
    categoryId: 1,
    photo: 'https://picsum.photos/id/43/500/500',
    title: 'Ad officia magna veniam sunt.'
  },
  {
    id: 3,
    categoryId: 3,
    photo: 'https://picsum.photos/id/722/500/500',
    title: 'Minim anim in sunt esse nisi sit magna consequat in sit laboris adipisicing.'
  },
  {
    id: 4,
    categoryId: 5,
    photo: 'https://picsum.photos/id/294/500/500',
    title: 'Deserunt in tempor est id consectetur cupidatat.'
  },
  {
    id: 72133,
    categoryId: 4,
    photo: 'https://picsum.photos/id/229/500/500',
    title: 'Labore culpa velit sunt sit anim ad do veniam do proident sunt et nisi mollit.'
  },
  {
    id: 5,
    categoryId: 1,
    photo: 'https://picsum.photos/id/862/500/500',
    title: 'Fugiat fugiat voluptate tempor minim ipsum nisi culpa magna officia ea deserunt tempor.'
  },
  {
    id: 62419,
    categoryId: 3,
    photo: 'https://picsum.photos/id/515/500/500',
    title: 'Excepted nisi aliquip ex aliqua consectetur id laboris cillum elit dolor dolor anim sint.'
  },
  {
    id: 6,
    categoryId: 5,
    photo: 'https://picsum.photos/id/730/500/500',
    title: 'Occaecat exercitation Lorem cupidatat adipisicing elit duis consequat esse et tempor eu enim cupidatat.'
  },
  {
    id: 47434,
    categoryId: 3,
    photo: 'https://picsum.photos/id/287/500/500',
    title: 'Veniam officia est nulla proident labore.'
  },
  {
    id: 7,
    categoryId: 3,
    photo: 'https://picsum.photos/id/859/500/500',
    title: 'Ut incididunt do magna culpa consectetur id deserunt et enim elit quis.'
  },
  {
    id: 8,
    categoryId: 5,
    photo: 'https://picsum.photos/id/110/500/500',
    title: 'Nisi velit fugiat voluptate fugiat magna officia qui fugiat ad non.'
  },
  {
    id: 9,
    categoryId: 5,
    photo: 'https://picsum.photos/id/649/500/500',
    title: 'Id ex enim non dolore reprehenderit eu ullamco.'
  },
];
const photo = createSlice({
  name: 'photos',
  initialState: initialPhotos,
  reducers: {
    addPhoto: (state, action) => {
      const newPhoto = action.payload;
      state.push(newPhoto)
    },
    removePhoto: (state, action) => {
      const removePhotoId = action.payload;
      return state.filter(photo => photo.id !== removePhotoId);
    },
    updatePhoto: (state, action) => {
      const newPhoto = action.payload;
      const photoIndex = state.findIndex(photo => photo.id === newPhoto.id)
      if (photoIndex >= 0) {
        state[photoIndex] = newPhoto;
      }
    }
  }
});
const { reducer, actions } = photo;
export const { addPhoto, removePhoto, updatePhoto } = actions;
export default reducer;
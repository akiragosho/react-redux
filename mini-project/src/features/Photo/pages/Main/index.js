import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { removePhoto } from 'features/Photo/photoSlice';
import { Banner, PhotoList } from 'components';
import  Images from 'constants/images';

function MainPage(props) {
  const dispatch = useDispatch()
  const photos = useSelector(state => state.photos)
  const history = useHistory();

  const handlePhotoEditClick = (photo) => {
    history.push(`/photos/${photo.id}`)
  }
  const handlePhotoRemoveClick = (photo) => {
    const action =  removePhoto(photo.id);
    dispatch(action)
  }
  return (
    <div className="photo-main">
      <Banner title="Photo App" backgroundUrl={Images.PINK_BG}></Banner>
      <PhotoList
        photoList={photos}
        onPhotoEditClick={handlePhotoEditClick}
        onPhotoRemoveClick={handlePhotoRemoveClick}
      />
    </div>
  );
}

export default MainPage;
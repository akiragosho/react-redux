import { Container } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { addPhoto, updatePhoto } from 'features/Photo/photoSlice';
import { useHistory, useParams } from 'react-router';
import { randomNumber } from 'utils/common';
import { PhotoForm, Banner } from 'components';

function AddEditPage(props) {
  const dispatch = useDispatch();
  //get id from router
  const { photoId } = useParams();
  const editedPhoto = useSelector(state => state.photos.find(x => x.id === +photoId))
  const isEdit = photoId;
  const initialValues = !isEdit
    ? {
      title: '',
      categoryId: null,
      photo: '',
    }
    : editedPhoto;
  const history = useHistory()
  const handleSubmit = (values) => {
    return new Promise(resolve => {
      setTimeout(() => {
        if (isEdit) {
          const action = updatePhoto(values);
          dispatch(action)
        } else {
          const newPhoto = {
            ...values,
            id: randomNumber(10, 99999)
          }
          const action = addPhoto(newPhoto);
          dispatch(action);
        }
        history.push('/photos');
        resolve(true)
      }, 1500)
    })

  }
  return (
    <div className="photo-edit">
      <Banner title="Edit Photo" />
      <Container className="mt-5">
        <PhotoForm
          isEdit={isEdit}
          initialValues={initialValues}
          onSubmit={handleSubmit} />
      </Container>
    </div>
  );
}

export default AddEditPage;
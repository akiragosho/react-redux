import React from 'react';
import { NotFound } from 'components';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
Photo.propTypes = {};
const MainPage = React.lazy(() => import('./pages/Main'));
const AddEditPage = React.lazy(() => import('./pages/AddEdit'));

function Photo(props) {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={MainPage} />
      <Route path={`${match.url}/add`} component={AddEditPage} />
      <Route path={`${match.url}/:photoId`} component={AddEditPage} />
      <Route component={NotFound} />
    </Switch>
  );
}

export default Photo;
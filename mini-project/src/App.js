import React, { Suspense, useEffect, useState } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Header, Menu } from 'components'
import './App.scss';
import productApi from './api/productApi'
//Lazy load - Code splitting
const Photo = React.lazy(() => import('./features/Photo'));
const NotFound = React.lazy(() => import('components'));

function App() {
  const [productList, setProductList] = useState([])
  useEffect(() => {
    const fetchProductList = async () => {
      try {
        const params = {
          _page: 1, 
          _limit: 10
        }
        const response = await productApi.getAll(params)
        console.log('response ', response);
        setProductList(response.data)
      } catch (err) {
        console.log('err ', err);
      }
    }
    fetchProductList();
  }, [])
  return (
    <div className="photo-app">
      <Suspense fallback={<div>Loading ...</div>}>
        <BrowserRouter>
          <Header />
          <Menu />
          <Switch>
            <Redirect exact from="/" to="/photos" />
            <Route path="/photos" component={Photo} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default App;

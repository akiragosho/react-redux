import { PHOTO_CATEGORY_OPTIONS } from 'constants/global';
import InputField from 'customFields/InputField';
import RandomPhotoField from 'customFields/RandomPhotoField';
import SelectField from 'customFields/SelectField';
import { FastField, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import { Button, FormGroup, Spinner } from 'reactstrap';
import * as Yup from 'yup';
import './style.scss'
PhotoForm.propTypes = {
  onSubmit: PropTypes.func,
  isEdit: PropTypes.string,
};

PhotoForm.defaultProps = {
  onSubmit: null,
  isEdit: ""
}

function PhotoForm(props) {
  const { initialValues, isEdit } = props
  const validationSchema = Yup.object().shape({
    title: Yup.string().required('This field is required.'),
    categoryId: Yup.number()
      .required('This field is required.')
      .nullable(),

    photo: Yup.string().when('categoryId', {
      is: 1,
      then: Yup.string().required('This field is required.'),
      otherwise: Yup.string().notRequired(),
    })
  });
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
      isEdit={props.isEdit}
    >
      {formikProps => {
        const { values, errors, touched, isSubmitting } = formikProps;
        return (
          <Form>
            <FastField
              name="title"
              component={InputField}
              label="Title: "
              placeholder="Enter title"
            />
            <FastField
              name="categoryId"
              component={SelectField}
              label="Category: "
              placeholder="What's your photo category?"
              options={PHOTO_CATEGORY_OPTIONS}
            />
            <FastField
              name="photo"
              component={RandomPhotoField}
              label="Photo: "
            />
            <FormGroup className="text-center">
              {
                isEdit
                  ? <Button className="btn-submit"type="submit" color="primary">
                    {isSubmitting && <Spinner size="sm" />}
                    Edit
                  </Button>
                  : <Button className="btn-submit"type="submit" color="danger">
                    {isSubmitting && <Spinner size="sm" />}
                    Add to album
                  </Button>
              }

            </FormGroup>
          </Form>
        );
      }}
    </Formik>
  );
}

export default PhotoForm;
import { NavLink } from 'react-router-dom';
import './style.scss'
function Menu(props) {
  return (
    <div>
      <ul className="list-menu">
        <li>
          <NavLink
            exact
            className="header__link"
            to="/photos"
            activeClassName="header__link--active"
          >
            List photos
          </NavLink>
        </li>
        <li>
          <NavLink
            exact
            className="header__link"
            to="/photos/add"
            activeClassName="header__link--active"
          >
            Add new photo
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Menu;
import PropTypes from 'prop-types';
import { Row, Col, Container } from 'reactstrap';
import { PhotoCard } from 'components';
import './style.scss'
PhotoList.propTypes = {
  photoList: PropTypes.array,
  onPhotoEditClick: PropTypes.func,
  onPhotoRemoveClick: PropTypes.func,
};

PhotoList.defaultProps = {
  photoList: [],
  onPhotoEditClick: null,
  onPhotoRemoveClick: null,
};

function PhotoList(props) {
  const { photoList, onPhotoEditClick, onPhotoRemoveClick } = props;

  return (
    <Container className="mt-5">
      <Row>
        {photoList.map(photo => (
          <Col key={photo.title} xs="12" md="6" lg="3">
            <PhotoCard
              photo={photo}
              onEditClick={onPhotoEditClick}
              onRemoveClick={onPhotoRemoveClick}
            />
          </Col>
        ))}
      </Row>
    </Container>

  );
}

export default PhotoList;
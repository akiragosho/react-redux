import Header from "./Header";
import Menu from "./Menu";
import NotFound from "./NotFound";
import Banner from "./Banner";
import PhotoList from "./PhotoList";
import PhotoCard from "./PhotoCard";
import RandomPhoto from "./RandomPhoto";
import PhotoForm from "./PhotoForm";

export {
  Header,
  Menu,
  NotFound,
  Banner,
  PhotoList,
  PhotoCard,
  RandomPhoto,
  PhotoForm,
}
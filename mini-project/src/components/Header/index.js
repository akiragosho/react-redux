import { NavLink } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';
import './style.scss'

Header.propTypes = {};

function Header() {
  return (
    <header className="header">
      <Container>
        <Row className="justify-content-between">
          <Col xs="auto">
            <a
              className="header__link header__title"
              href="https://www.facebook.com/AKIRAGOSHO/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Akira Gosho
            </a>
          </Col>
        </Row>
      </Container>
    </header>
  );
}

export default Header;